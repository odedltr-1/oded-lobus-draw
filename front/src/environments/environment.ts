export const environment = {
  production: false,
  hostServer: 'http://localhost:3000/api/v1/',
  imageUrl : 'https://firebasestorage.googleapis.com/v0/b/lobus-backend-draws.appspot.com/o/__name__?alt=media&token='
};
