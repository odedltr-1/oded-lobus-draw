export const environment = {
  production: true,
  hostServer: 'http://ec2-34-213-207-144.us-west-2.compute.amazonaws.com:3000/api/v1/',
  imageUrl : 'https://firebasestorage.googleapis.com/v0/b/lobus-backend-draws.appspot.com/o/__name__?alt=media&token='
};
