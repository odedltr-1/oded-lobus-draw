(function () {

  'use strict';

  var burgerMenu = function() {
    $('body').on('click', '.js-fh5co-nav-toggle', function(event) {
      if ($('#navbar').is(':visible')) {
        $(this).removeClass('active');
      } else {
        $(this).addClass('active');
      }
      event.preventDefault();
    });
  };

  var clickMenu = function() {
    $('a:not([class="external"])').click(function() {
      var section = $(this).data('nav-section'),
        navbar = $('#navbar');
      setTimeout(function() {
        if ($('[data-section="' + section + '"]').length) {
          var top = $('[data-section="' + section + '"]').offset().top;
          $('html, body').animate({
            scrollTop: top
          }, 500);
          if (navbar.is(':visible')) {
            sketchesWayPoint();
            navbar.removeClass('in');
            navbar.attr('aria-expanded', 'false');
            $('.js-fh5co-nav-toggle').removeClass('active');
          }
        }
      }, 2000);
    });
  };

  var windowScroll = function() {
    $(window).scroll(function() {
      var header = $('#fh5co-header'),
        scrlTop = $(this).scrollTop();
      if (scrlTop > 500 && scrlTop <= 2000) {
        header.addClass('navbar-fixed-top fh5co-animated slideInDown');
      } else if (scrlTop <= 500) {
        if (header.hasClass('navbar-fixed-top')) {
          header.addClass('navbar-fixed-top fh5co-animated slideOutUp');
          setTimeout(function() {
            header.removeClass('navbar-fixed-top fh5co-animated slideInDown slideOutUp');
          }, 100);
        }
      }
    });
  };

  var sketchesAnimate = function() {
    if ($('#fh5co-our-sketches').length > 0) {
      $('#fh5co-our-sketches .to-animate').each(function(k) {
        var el = $(this);
        setTimeout(function() {
          el.addClass('fadeInUp animated');
        },  k * 200, 'easeInOutExpo');
      });
    }
  };

  var sketchesWayPoint = function() {
    $('#fh5co-our-sketches').waypoint(function(direction) {
      if(direction === 'down' && !$(this).hasClass('animated')) {
        setTimeout(sketchesAnimate, 1000);
        $(this.element).addClass('animated');
      }
    }, {offset: '95%'});
  };

  $(function() {
    burgerMenu();
    clickMenu();
    windowScroll();
    sketchesWayPoint();
  });
}());
