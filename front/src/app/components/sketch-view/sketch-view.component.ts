import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-sketch-view',
  template: '.<img class="newImage" src="{{createImageUrl()}}" />'
})

/**
 * Display a single sketch.
 */
export class SketchViewComponent {

  name: string;
  token: string;

  constructor(private aRoute: ActivatedRoute) {
    this.name = aRoute.snapshot.params['name'];
    this.token = aRoute.snapshot.params['token'];
  }

  createImageUrl() {
    return environment.imageUrl.replace('__name__', this.name) + this.token;
  }
}
