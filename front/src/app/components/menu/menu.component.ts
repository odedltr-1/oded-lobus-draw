import { Component } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuComponent {

  // Indicate current (active) tab.
  activeTab: string;

  constructor() {
    this.activeTab = 'ourSketches';
  }
}
