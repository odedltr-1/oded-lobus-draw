import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-our-sketches',
  templateUrl: './our-sketches.component.html',
  styleUrls: ['./our-sketches.component.scss']
})

export class OurSketchesComponent implements OnInit {

  sketches;
  sketchesKeys = [];

  constructor(private _httpService: HttpService) {}

  ngOnInit() {
    // Ref to this component to use from inside _httpService callback.
    const that = this;

    that._httpService.list().subscribe(
      res => {
        if (res['result'] === 0) {
          console.log('Successfully retrieved sketches.', res);
          that.sketches = res['sketches'];
          that.sketchesKeys =
            res['sketches'] ? Object.keys(res['sketches']) : [];
        }
      },
      err => {
        console.log('Error occurred while retrieved sketches: ', err);
      });
  }
}
