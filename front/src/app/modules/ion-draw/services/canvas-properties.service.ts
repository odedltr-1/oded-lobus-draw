import { Injectable } from '@angular/core';

@Injectable()
/**
 * Holds all available configuration for canvas properties.
 */
export class CanvasPropertiesService {

  availableColors: any;
  availableSizes: any;

  constructor() {
    this.availableColors = {
      'LIGHT_BLUE': '#6173f4',
      'PINK': '#f64662',
      'BLUE': '#2185d5',
      'TURQUOISE': '#00b8a9',
      'ORANGE': '#ff6600',
      'PURPLE': '#5585b5',
      'RED': '#a03232',
      'GREEN': '#65d269'
    };
    this.availableSizes = [5, 10, 20, 50];
  }
}
