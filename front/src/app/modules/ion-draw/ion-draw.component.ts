import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePageComponent } from './pages/home/home';

@Component({
  templateUrl: 'ion-draw.html',
  selector: 'app-draw-wrap'
})

/**
 * Since this Ionic module is page based - this component hold the page that
 * displays the canvas.
 */
export class IonDrawComponent {
  // Pointing to the page container of the module.
  rootPage: any = HomePageComponent;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}

