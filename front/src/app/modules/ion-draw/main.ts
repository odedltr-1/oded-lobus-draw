import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { IonDrawModule } from './ion-draw.module';

platformBrowserDynamic().bootstrapModule(IonDrawModule);
