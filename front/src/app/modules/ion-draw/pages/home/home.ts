import { Component } from '@angular/core';

@Component({
  selector: 'app-page-home',
  templateUrl: 'home.html'
})

/**
 * Displays the canvas painter.
 */
export class HomePageComponent {}
