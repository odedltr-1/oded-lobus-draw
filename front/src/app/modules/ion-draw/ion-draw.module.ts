import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { IonDrawComponent } from './ion-draw.component';
import { HomePageComponent } from './pages/home/home';
import { CanvasDrawComponent } from './components/canvas-draw/canvas-draw';
import { ColorPickerComponent } from './components/color-picker/color-picker';
import { CanvasSubmitComponent } from './components/canvas-submit/canvas-submit';

import { CanvasPropertiesService } from './services/canvas-properties.service';
import { HttpService } from '../../services/http.service';

/**
 * This is an Ionic module that encapsulates Ionic canvas painter.
 */
@NgModule({
  declarations: [
    IonDrawComponent,
    HomePageComponent,
    CanvasDrawComponent,
    ColorPickerComponent,
    CanvasSubmitComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(IonDrawComponent)
  ],
  exports: [IonDrawComponent],
  bootstrap: [IonicApp],
  entryComponents: [
    IonDrawComponent,
    HomePageComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CanvasPropertiesService,
    HttpService
  ]
})

export class IonDrawModule {}
