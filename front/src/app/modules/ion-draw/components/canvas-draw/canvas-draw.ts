import { Component, ViewChild, Renderer2, AfterViewInit } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';

import { CanvasPropertiesService } from '../../services/canvas-properties.service';

@Component({
  selector: 'app-canvas-draw',
  templateUrl: 'canvas-draw.html'
})

/**
 * Initialize the canvas Manages all activity for painting the stroke.
 */
export class CanvasDrawComponent implements AfterViewInit {

  // Pointer to canvas element.
  @ViewChild('canvasDraw') canvasDraw: any;
  lastX: number;
  lastY: number;
  mouseClicked: boolean;
  isMobile: boolean;
  // Object to hold canvas configuration: color & size.
  config: object;
  // Start time of sketch.
  startTimeStamp: number;

  constructor(
    private _platform: Platform,
    private _renderer: Renderer2,
    private _canvasProperties: CanvasPropertiesService) {
    this.mouseClicked = false;
    // Initially Setting stroke's properties
    this.config = {
      color: _canvasProperties.availableColors.BLUE,
      size: _canvasProperties.availableSizes[0]
    };
  }

  ngAfterViewInit() {
    this._renderer.setAttribute(
      this.canvasDraw.nativeElement, 'width', this._platform.width() + ''
    );
    this._renderer.setAttribute(
      this.canvasDraw.nativeElement, 'height', this._platform.height() + ''
    );

    // Mouse click & touch event listener.
    Observable.merge(
      Observable.fromEvent(this.canvasDraw.nativeElement, 'mousedown'),
      Observable.fromEvent(this.canvasDraw.nativeElement, 'touchstart')
    ).subscribe(event => {
        this.handleStrokeStart(event);
      });

    // Mouse & touch move event listener.
    Observable.merge(
      Observable.fromEvent(this.canvasDraw.nativeElement, 'mousemove'),
      Observable.fromEvent(this.canvasDraw.nativeElement, 'touchmove')
    ).subscribe(event => {
      this.handleStrokeMove(event);
    });

    // Mouse & touch release event listener.
    Observable.merge(
      Observable.fromEvent(this.canvasDraw.nativeElement, 'mouseup'),
      Observable.fromEvent(this.canvasDraw.nativeElement, 'touchend')
    ).subscribe(() => {
      this.handleStrokeEnd();
    });
  }

  /**
   * Callback for stroke start.
   * @param {Object} event
   */
  handleStrokeStart(event) {
    // Ignore right click for Chrome, Firefox, Safari.
    if (event.which === 3) {
      return;
    }

    // If this is first click/touch on the canvas.
    if (!this.startTimeStamp) {
      // Keep start time.
      this.startTimeStamp = Date.now();
      // Indicate if user agent is a mobile phone.
      this.isMobile = !!event.touches;
    }

    // Ignore mobile event on desktop, for example Chrome mobile emulator.
    if (this.isMobile) {
      if (!event.touches) {
        return;
      }
    }

    this.mouseClicked = true;
    this.lastX = this.isMobile ? event.touches[0].clientX : event.layerX;
    this.lastY = this.isMobile ? event.touches[0].clientY : event.layerY;
  }

  /**
   * Callback for stroke continue.
   * @param {Object} event
   */
  handleStrokeMove(event) {
    // Only when the mouse is click we want to draw the stroke.
    if (this.mouseClicked) {
      const newX = this.isMobile ? event.touches[0].clientX : event.layerX;
      const newY = this.isMobile ? event.touches[0].clientY : event.layerY;
      const ctx = this.canvasDraw.nativeElement.getContext('2d');
      ctx.beginPath();
      ctx.lineJoin = 'round';
      ctx.moveTo(this.lastX, this.lastY);
      ctx.lineTo(newX, newY);
      ctx.strokeStyle = this.config['color'];
      ctx.lineWidth = this.config['size'];
      ctx.closePath();
      ctx.stroke();
      this.lastX = newX;
      this.lastY = newY;
    }
  }

  /**
   * Callback for stroke end.
   */
  handleStrokeEnd() {
    this.mouseClicked = false;
  }

  /**
   * Handle color/size button click in the color picker.
   * @param config The color & size to use with the stroke.
   */
  onUpdateCanvas(config) {
    this.config = config;
    this.mouseClicked = false;
  }

  /**
   * Handle clear canvas button click in the color picker.
   */
  onClearCanvas() {
    const ctx = this.canvasDraw.nativeElement.getContext('2d');
    const nativeElement = this.canvasDraw.nativeElement;
    // Clear the stroke.
    ctx.clearRect(0, 0, nativeElement.clientWidth, nativeElement.height);
  }
}
