import { Component, Input } from '@angular/core';
import { HttpService } from '../../../../services/http.service';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-canvas-submit',
  templateUrl: 'canvas-submit.html',
  styleUrls: ['./canvas-submit.scss']
})

/**
 * Presents a form for submit current sketch.
 */
export class CanvasSubmitComponent {

  // Start time of sketch.
  @Input() startTimeStamp: number;

  showForm: boolean;
  // Form fields
  nameUser: string;
  nameSketch: string;
  isPrivate: boolean;

  constructor(private _httpService: HttpService) {}

  /**
   * Submit handler for paint submit form.
   * @param form Values collection
   */
  onSubmit(form: any) {
    // Ref to this component to use from inside html2canvas callback.
    const that = this;

    html2canvas(document.getElementById('canvasDraw'), {
      background: '#ffffff', dpi: 192,
      onrendered: function(canvas) {
        // Merge start time & new file with values to pass to the HTTP request.
        that._httpService.add(Object.assign(form, {
          timeStamp: that.startTimeStamp,
          fileBase64: canvas.toDataURL('image/jpeg').
            replace(/^data:image\/jpeg;base64,/, ''),
        })).subscribe(
          res => {
            console.log('Successfully added entry.', res);
            if (that.isPrivate) {
              // Popping image's URL if user requested it tp be private.
              // using an alert is the easy solution.
              /* TODO: embed in ionModal */
              alert('imageURL: ' + res['imageURL']);
            }
            /* TODO this is an easy fix using this.router.navigate(['']) */
            /* TODO due to an issue with animation script. */
            document.getElementById('our-sketches').click();
          },
          err => {
            console.log('Error occurred while adding an entry: ', err);
          });
      }
    });
  }
}
