import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CanvasPropertiesService } from '../../services/canvas-properties.service';

@Component({
  selector: 'app-color-picker',
  templateUrl: 'color-picker.html',
  styleUrls: ['./color-picker.scss']
})

/**
 * Enables color selection for the stroke.
 */
export class ColorPickerComponent {

  showPicker: boolean;
  colorsKeys: any;
  @Output() updateCanvasEmit: EventEmitter<any> = new EventEmitter<any>();
  @Output() clearCanvasEmit: EventEmitter<any> = new EventEmitter<any>();
  // Object to hold canvas configuration: color & size.
  @Input() config: object;

  // Start time of sketch.
  @Input() startTimeStamp: number;

  constructor(public canvasProperties: CanvasPropertiesService) {
    this.showPicker = true;
    // Keys for available colors to use in html (*ngFor doesn't support objects)
    this.colorsKeys = Object.keys(canvasProperties.availableColors);
  }

  /**
   * Handle color button click.
   * @param color The color to use with the stroke.
   */
  updateColor(color) {
    this.config['color'] = color;
    // Updating parent component.
    this.updateCanvasEmit.emit(this.config);
  }

  /**
   * Handle size button click.
   * @param size The size to use with the stroke.
   */
  updateSize(size) {
    this.config['size'] = size;
    // Updating parent component.
    this.updateCanvasEmit.emit(this.config);
  }

  /**
   * Handle clear canvas button click.
   */
  clearCanvas() {
    // Updating parent component.
    this.clearCanvasEmit.emit();
  }
}
