import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { OurSketchesComponent } from './components/our-sketches/our-sketches.component';
import { SketchViewComponent } from './components/sketch-view/sketch-view.component';
import { IonDrawModule } from './modules/ion-draw/ion-draw.module';
import { HttpService } from './services/http.service';
import { IonDrawComponent } from './modules/ion-draw/ion-draw.component';

const appRoutes: Routes = [
  { path: 'our-sketches', component: OurSketchesComponent },
  { path: 'our-sketches/:name/:token', component: SketchViewComponent },
  { path: 'try-for-free', component: IonDrawComponent },
  { path: '**', redirectTo: 'our-sketches' }
];

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    OurSketchesComponent,
    SketchViewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonDrawModule,
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  providers: [HttpService],
  bootstrap: [AppComponent]
})

export class AppModule {}
