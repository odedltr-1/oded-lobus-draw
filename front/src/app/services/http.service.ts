import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
/**
 * This service is the gateway to all server calls.
 */
export class HttpService {

  constructor(private http: HttpClient) {}

  /**
   * Server call to get list of all sketches from the database.
   * @returns {Observable<Object>}
   */
  list() {
    return this.http.get(environment.hostServer + 'list');
  }

  /**
   * Server call to add sketch to the database.
   * @param {Object} formValues Details for the new entry as entered by user.
   * @returns {Subscription} On success return:
   *                         1. The new entry, or
   *                         2. Only URL for the sketch, if user request private.
   *                         On failure return: error message.
   */
  add(formValues: Object) {
    return this.http.post(
      environment.hostServer + 'add',
      JSON.stringify(formValues),
      {headers: new HttpHeaders({'Content-Type': 'application/json'})}
    );
  }
}
