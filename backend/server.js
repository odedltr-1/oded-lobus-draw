'use strict';

const express = require('express');
const app = express();

// Body parser mw.
const bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// Globally store source path.
global.appSrc = require('path').resolve(__dirname) + '/src/';

const constants = require(global.appSrc + 'lib/constants');
const logger = require(global.appSrc + 'lib/logger');

// to enable cross-origin resource sharing
const cors = require('cors');
app.use(cors());

// routing
let mainRouter = require(global.appSrc + 'routes/mainRouter');
app.use('/api/v1', mainRouter);

try {
  app.listen(constants.PORT, function () {
    logger.info('Listening on port ' + constants.PORT + ' ...');
  })
} catch (error) {
  logger.error(
    'Error caught while listening on port ' + constants.PORT, error
  );
}