'use strict';

const logger = require(global.appSrc + 'lib/logger');
// DB connection.
const instance = require('firebase').initializeApp(
  require(global.appSrc + 'lib/constants').FIREBASE_CONFIG
);

module.exports = {

  /**
   * Retrieve sketches entries from Firebase DB.
   *
   * @returns {PromiseLike<any>}
   */
  listRecords: function() {
    return instance.database().ref().child('drawings').
      orderByChild('isPrivate').equalTo(false).once('value')
        .then(function(snapshot) {
          return snapshot.val();
        })
  },

  /**
   * Add a sketch entry to Firebase DB.
   *
   * @param recordObject Desired object to keep in the DB, after sanitation.
   * @returns {PromiseLike<any>}
   */
  addRecord: function(recordObject) {
    return instance.database().ref().child('drawings')
      .push(recordObject)
        .then(function() {
          instance.database().ref().child('drawings').limitToLast(1).
            on('child_added', function(snapshot) {
              return snapshot.val();
            })
        })
  },

  /**
   * Save image locally *** NOT IN USE ***
   *
   * @param base64Data Base64 representation of the file to keep.
   * @returns {Promise<any>}
   */
  saveImage: function(base64Data) {
    return new Promise(function(resolve, reject) {
      require('fs').writeFile('upload/sketch.jpeg', base64Data, 'base64',
        function(err) {
          if (err) {
            reject(err);
          }
          else {
            logger.info('Successfully saved file.');
            resolve();
          }
      });
    });
  },

  /**
   * Upload image to Firebase storage.
   *
   * @param base64Data Base64 representation of the file to keep.
   * @returns {Promise<any>}
   */
  uploadImage: function(base64Data) {
    return new Promise((resolve, reject) => {
      // Retrieve storage bucket.
      const storage = require('@google-cloud/storage')({
        projectId: require(global.appSrc + 'lib/constants').FIREBASE_CONFIG.projectId,
        keyFilename: global.appSrc + 'lib/419f2459c8.json'
      });
      const bucket = storage.bucket('lobus-backend-draws.appspot.com');

      // Create stream for the reading the file.
      const stream = require('stream');
      const bufferStream = new stream.PassThrough();
      bufferStream.end(new Buffer.from(base64Data, 'base64'));

      // Generate keys for assemble file name and token.
      const fileName = 'sketch_' + (new (require('./timeStamp'))('file'));
      const token = require('uuidv4')();

      // Create a reference to the new image file.
      const file = bucket.file(fileName);

      // Pipe the file stream to the bucket.
      bufferStream.pipe(file.createWriteStream({
        uploadType: "media",
        metadata: {
          contentType: 'image/jpeg',
          metadata: {
            firebaseStorageDownloadTokens: token
          }
        }
      }))
        .on('error', error => {
          logger.error('Error while uploading picture', error);
          reject();
      })
        .on('finish', () => {
          // The file upload is complete.
          logger.info('Successfully uploaded image.');
          resolve({name: fileName, token: token});
        });
    })
  }
};