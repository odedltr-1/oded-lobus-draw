module.exports = Object.freeze({
  FIREBASE_CONFIG: {
    apiKey: "AIzaSyBfg8YLlWmdb418m1OFiwWuV3AoGkZIJjw",
    authDomain: "lobus-backend-draws.firebaseapp.com",
    databaseURL: "https://lobus-backend-draws.firebaseio.com",
    projectId: "lobus-backend-draws",
    storageBucket: "lobus-backend-draws.appspot.com",
    messagingSenderId: "426869498505"
  },
  IMAGE_URL : 'https://firebasestorage.googleapis.com/v0/b/lobus-backend-draws.appspot.com/o/__name__?alt=media&token=',
  PORT: 3000
});