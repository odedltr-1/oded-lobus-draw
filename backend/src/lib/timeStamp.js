'use strict';

const dateFormat = require('dateformat');

/**
 * A pretty print of current time.
 *
 * @param op The requested style, Expected 'log' or 'file'
 */
let timeStamp = function (op) {

  Object.defineProperty(this, 'op', {
    get: function () {
      return op;
    }
  });

  this.getTimeLog = function () {
    return dateFormat(new Date(), "dd/mm/yyyy HH:MM:ss");
  };

  this.getTimeFile = function () {
    return dateFormat(
      new Date(), 'yyyymmdd') + '_' + dateFormat(new Date(), "HHMMss"
    );
  };

};

timeStamp.prototype.toString = function () {
  switch (this.op) {
    case 'log':
      return this.getTimeLog();
    case 'file':
      return this.getTimeFile();
  }
};

module.exports = timeStamp;