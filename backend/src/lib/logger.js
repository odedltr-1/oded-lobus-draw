'use strict';

const winston = require('winston');
const config = winston.config;

/**
 * Customize logger messages.
 */
module.exports = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      timestamp: function() {
        return new (require('./timeStamp'))('log');
      },
      formatter: function(options) {
        // string that will be passed to logger
        return config.colorize(options.level, options.level.toUpperCase() +
          ' --- ') + options.timestamp() + ' ' +
          (options.message ? options.message : '') +
          (options.meta && options.meta.message ? ': ' + options.meta.message: '');
      },
    })
  ]
});