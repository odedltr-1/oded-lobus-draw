let express = require('express');
let router = express.Router();

let mainController =
  require(global.appSrc + '/controllers/api/v1/mainController');

router.route('/list').get(mainController.listRecords);
router.route('/add').post(mainController.addRecord);

module.exports = router;