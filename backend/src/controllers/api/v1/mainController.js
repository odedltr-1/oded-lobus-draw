'use strict';

const firebase = require(global.appSrc + 'lib/firebase');
const logger = require(global.appSrc + 'lib/logger');

module.exports = {
  /**
   * Handler for get request to get sketches list.
   */
  listRecords: function(req, res) {
    firebase.listRecords().then(snapshot => {
      logger.info('Successfully retrieved sketches.');
      res.json({result: 0, sketches: snapshot});
    }).catch(error => {
      logger.error('Failed retrieving sketches' , error);
      res.json({result: 1, error: error.toString()});
    });
  },

  /**
   * Handler for post request to add sketch record to the DB.
   * Returns json success: {result: 0}, error: {result: 1}
   */
  addRecord: function(req, res) {
    // First save the file. If file is missing return error.
    if (!req.body.fileBase64) {
      logger.error('No file was uploaded');
      return res.json({result: 1, error: 'No file was uploaded.'});
    }
    // Save file. If file successfully save firebase.uploadImage will return
    // its name & token, and we will continue to save the corresponding data.
    firebase.uploadImage(req.body.fileBase64).then(function(data) {
      // If file upload was successful, now save the other details.
      // Validate and add the record to the firebase DB.
      const recordObject = makeRecordObject(req.body, data);
      firebase.addRecord(recordObject).then(() => {
        // On success - send client the new URL of the image.
        logger.info('Successfully added record.');
        res.json({result: 0, imageURL: recordObject.image.url});
      }).catch(error => {
        logger.error('Failed adding record' , error);
        res.json({result: 1, error: error.toString()});
      });
    }).catch(function(error) {
      logger.error('Failed adding record' , error);
      res.json({result: 1, error: error.toString()});
    });
  }
};

/**
 * Sanitise and validate request object for creating a new sketch entry.
 * Setting default values for fields which are not validated.
 *
 * @param {Object} reqBody Form fields collection from the submitted sketch.
 * @param {Object} fileUrlHints fileName & token for the newly save file.
 * @returns {*}
 */
function makeRecordObject(reqBody, fileUrlHints) {
  // This is the desired model for the sketch record. We take only these
  // properties form the post request and ignore anything else.
  // If values didn't survive sanitation - select the default values.
  return {
    nameUser: (reqBody.hasOwnProperty('nameUser') ?
      reqBody.nameUser.replace(/[^\w\s]/gi, '').trim() : '')  || 'Some Dude',

    nameSketch: (reqBody.hasOwnProperty('nameSketch') ?
      reqBody.nameSketch.replace(/[^\w\s]/gi, '').trim() : '')  || 'Some Sketch',

    isPrivate: !!reqBody.isPrivate,

    creationTime: Date.now(),

    // If startTimeStamp is validated - calc the duration.
    duration: (new Date(reqBody.timeStamp)).getTime() > 0 ?
      (Date.now() - reqBody.timeStamp) : 0,

    // Create full URL for the corresponding file.
    image: Object.assign({
      url: require(global.appSrc + 'lib/constants').IMAGE_URL.
      replace('__name__', fileUrlHints.name) + fileUrlHints.token
    }, fileUrlHints)
  };
}