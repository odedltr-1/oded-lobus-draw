# Lobus - Full Stack Assignment

Hello! This repository holds the required assets for the task that I was requested
to do as part of my interviewing process.
You can find here both the backend & front source files for the project.

# Assignment Specification

This is a web site that enables the user to create his own sketches and upload them
to a public DB and view previous sketches.

# Last Update
May 16, 2018.

## Live Production URL

[AWS](http://ec2-34-213-207-144.us-west-2.compute.amazonaws.com)

## UI Support

**Desktop** Successfully tested on Chrome, Firefox & Safari.

**Mobile** Successfully tested on iPhone Safari, Chrome Emulator for iPad.

# Architecture

## Backend - Node.JS v. 10

The main idea is to expose an API the enables the client (browser, app) to retrieve
and save images (and maybe more in the future).
The flow for handling a request: routers => controller => lib => controller => response.

### Dependency between components
* backend/server.js is the main process.
* backend/src/routes is the where the server meets client requests. Each request
is being assigned by the router to the corresponding action in the controller.
* backend/src/controllers/api/v1 - the latest API for handling requests. The controllers
are meant to be 'thin', which means - contain minimum logic and their main job is to
return a response to the client.
* backend/src/lib - the business logic behind the server and helpers. Including a
Database layer firebase.js.

### Flow
Currently, backend's API V1 supports 2 operations:

|  action  |  http type  |   api url     |      purpose      |
| -------- |:-----------:| :------------:| -----------------:|
|   list   |     get     |   api/v1/list |  get all records  |
|   add    |     post    |   api/v1/add  |  add a record     |

** List - retrieve all stored records **

1. mainRouter => mainController.listRecords()
2. mainController.listRecords() => firebase.listRecords()
3. firebase.listRecords() => Firebase API # requesting all images that are not 'private'
   (later on 'private' in the front).
4. Firebase API => firebase.listRecords() => mainController.listRecords() => client.

** Add - add a record **

1. mainRouter => mainController.addRecord() # validate existence of file.
2. mainController.addRecord() => firebase.uploadImage() # Firebase exposes 2 different APIs for DB and storage.
   So we need to have 2 requests to Firebase:
   A. First save the file on the storage. If successful and we get a file link, then
   B. Save the corresponding image's details on the database (name, time, image url...).
   So prior to save the whiole entry, the mainController will verify that the image has been saved correctly.
3. firebase.uploadImage() => mainController.addRecord() # If image upload was successful, the controller receives
   the newly saved file name & token and will pre-prepossess the object to save.
   mainController.makeRecordObject - creates a validated & complete object from the http request.
4. mainController.addRecord() => firebase.addRecord() # Now firebase will save
   the whole object with all the new entry's details.
   An example for a saved entry as it is stored on the Firebase database:
```
-LCaroVSnJ4_8dsarOd4
    creationTime: 1526439299135
    duration: 8547
    image
        name: "sketch_20180515_225457"
        token: "c4d2232c-36cb-4eae-b588-e7b121d1842c"
        url: "https://firebasestorage.googleapis.com/v0/b/lob..."
    isPrivate: false
    nameSketch: "Some Sketch"
    nameUser: "Some Dude"
```

## Front - Angular 6

### Screens

1. Our Sketches - user may browse through a gallery of all sketches that were created by the users. Each sketch also expose the following data:
    1. Sketch's name.
    2. Creator's name.
    3. Time of creation.
    4. Duration.
Clicking on a sketch will direct to the next screen:
2. Single Sketch - will present the stand alone sketch.
3. Try For Free - This is the canvas where the user may create his own sketch & upload it. Note the toolkit on the left. Its functions:
    1. User may use a toolkit for different drawing capabilities.
    2. Set name for his sketch.
    3. Upload image.
    4. Get a URL for the image (need to check 'private').

### Structure

2 Modules:

1. **The traditional AppModule** - Contains the menu, gallery and footer areas.
It is also encapsulating the canvas module.
Among the the AppModule responsibilities is managing the app's routing.
The routing is hash based to prevent reloading the app when navigating.

    ### Components
        * MenuComponent - Enable routing between gallery area and canvas area.
          Interacting with HttpService when requesting all past images.
        * OurSketchesComponent - The default component which is displayed to the user on load.
          Contains all the recent images and their data. Clicking on an image will lead to:
        * SketchViewComponent - An individual view for a single image. The click event from the gallery
          passes the image's details, so SketchViewComponent can compose a URL and present it.
    ### Services
        * HttpService - manages all outgoing requests to the backend.

2. **IonDrawModule** - this module is basically an Ionic module, created originally as a stand alon Ionic project and then imported to this project. It encapsulate all the needed components for the canvas.

    ### Components
        * CanvasDrawComponent - Canvas area. Basically it is the playground for painting.
        * ColorPickerComponent - A toolkit for sketching options. Already explained in **Screens**. Also encapsulates:
        * CanvasSubmitComponent - Final submission of the sketch. User can decide on 3 params:
            1. Sketch name
            2. User name
            3. Is private: A user may decide he doesn't want his image to be on the gallery. In this case he will have only the link to share.
    ### Services
        * CanvasPropertiesService - manages all outgoing requests to the backend.

## Tradeoffs and known issues
The following are known issues that are not yet fixed, due to time management.

1. Some warning might be found on the console. They are a result of NPM incompatibility packages
   between themselves. But the app works.
2. The app UI is based on a Bootstrap theme [Crew](https://freehtml5.co/crew-free-html5-bootstrap-template/)
   Therefore, there is some very little jQuery in this project, only for the animations.
3. When a user is being present to his image's URL - it pops up with a usual
   alert box. When have the time, I will use IonModal.
4. When clicking in the menu on the Our Sketches there might be a console error;
```
'invalid views to insert'
```
   It is happening because an issue with Ion Page, since this menu item leads to a
   routing that contains an Ion Page. There is an [open issue](https://github.com/ionic-team/ionic/issues/13759).
5. In some cases the Animation is not working correctly. So for example, when viewing a stand alone image
   and hitting back button - the image will be pulled to the app, but not presented.


## Installing
Backend. from root folder:
```
cd backend
npm install
npm start
```

Front. from root folder:
```
cd front
npm install
ng serve
```

## Author

Oded Alter | odedltr@gmail.com | ‭+1 (917) 407-1030 | [bitbucket](https://bitbucket.org/odedltr-1/)

## License

MIT